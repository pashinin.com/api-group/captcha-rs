use actix::Addr;
use chrono::Duration;
use actix_redis::{RedisActor, Command};
use redis_async::resp_array;
use redis_async::resp::{FromResp};
use tracing::error;

#[cfg(not(debug_assertions))]
use std::env;

/// Key format to store captcha info
/// By default: "captcha-<UUID>"
fn key(uuid: impl ToString) -> String {
    format!("captcha-{}", uuid.to_string())
}

/// Captcha storage implemented using Redis
#[derive(Clone)]
pub struct Redis {
    actor: Addr<RedisActor>,
}

impl Redis {
    /// INSERT a pair (captcha_uuid, secret) into storage
    pub async fn set(&self, uuid: impl ToString, secret: impl ToString, ttl: Duration) {
        let cmd = Command(resp_array![
            "SET",
            key(uuid),
            secret.to_string(),
            "EX",
            ttl.num_seconds().to_string(),
        ]);
        match self.actor.send(cmd).await {
            Ok(_) => {},
            Err(err) => error!("{}", err),
        };
    }

    /// Gets captcha's secret by UUID
    pub async fn get(&self, uuid: impl ToString) -> Option<String> {
        match self.actor.send(Command(resp_array![
            "GET", key(uuid)
        ])).await.unwrap()
        {
            // Got response from Redis, try to convert to String.
            // Can't convert "Nil" for example.
            Ok(val) => match String::from_resp_int(val) {
                Ok(s) => Some(s),
                Err(_) => None,
            },
            Err(err) => {
                error!("{}", err);
                None
            }
        }
    }

    /// Checks if connection is ok
    pub async fn connection_ok(&self) -> bool {
        match self.actor
            .send(Command(resp_array!["GET", "captcha-"]))
            .await
            .unwrap()
        {
            Ok(_v) => true,
            Err(err) => {
                error!("{}", err);
                false
            }
        }
    }

    /// Returns true when given a valid secret
    pub async fn verify<S: ToString>(&self, captcha_uuid: S, captcha_value: S) -> bool {
        match self.get(captcha_uuid).await {
            Some(secret) => secret == captcha_value.to_string(),
            None => false,
        }
    }
}

impl Default for Redis {
    fn default() -> Self {
        Self {
            #[cfg(not(debug_assertions))]
            actor: RedisActor::start(
                env::var("REDIS").unwrap_or(String::from("localhost:6379"))),
            #[cfg(debug_assertions)]
            actor: RedisActor::start("redis:6379"),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[actix_rt::test]
    async fn test_set_get() {
        let s = Redis::default();
        assert_eq!(s.connection_ok().await, true);

        s.set("test", "test", Duration::seconds(5)).await;
        assert_eq!(s.verify("test", "test").await, true);
    }
}
