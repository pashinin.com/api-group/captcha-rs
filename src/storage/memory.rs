use actix::Addr;
// use actix::prelude::*;
// use dashmap::DashMap;
use uuid::{Uuid};
// use std::time::{Duration, Instant};
// use chrono::{DateTime, Utc};
// use chrono::prelude::*;
use chrono::Duration;
// use redis::Commands;
use actix_redis::{RedisActor, Command};
use redis_async::resp_array;
use redis_async::resp::RespValue;
// use crate::models::Item;

pub trait Storage {
    // fn new() -> impl Storage;
    fn set(&self, uuid: Uuid, secret: String, ttl: Duration);
    fn get(&self, uuid: &Uuid) -> Option<String>;
    // fn len(&self) -> usize;
}


// #[derive(Debug)]
// pub struct InMemoryItem {
//     pub secret: String,
//     pub expired_at: DateTime<Utc>,
// }

// #[derive(Debug,Clone)]
// pub struct InMemory {
//     map: Arc<DashMap<uuid::Uuid, InMemoryItem>>,
// }

// impl InMemory {
//     fn cleanup(&self) {
//         let mut old: Vec<Uuid> = vec![];
//         for t in self.map.iter() {
//             if Utc::now() > t.value().expired_at {
//                 old.push(*t.key());
//             }
//         }
//         for key in old.iter() {
//             self.map.remove(key);
//         }
//     }
// }

// impl Storage for InMemory {
//     // fn len(&self) -> usize {
//     //     self.map.len()
//     // }

//     /// INSERT a captcha pair: (uuid, secret)
//     fn set(&self, uuid: Uuid, secret: String, ttl: Duration) {
//         self.map.insert(uuid, InMemoryItem{
//             secret: secret,
//             expired_at: Utc::now() + ttl,
//         });
//         self.cleanup();
//     }

//     /// Gets captcha's secret by UUID
//     fn get(&self, uuid: &Uuid) -> Option<String> {
//         let mut delete = false;
//         let result = match self.map.get(uuid) {
//             Some(data) => {
//                 let item = &*data.value();
//                 if Utc::now() >= item.expired_at {
//                     // can't delete right here while using data
//                     delete = true;
//                     None
//                 } else {
//                     Some(item.secret.clone())
//                 }
//             },
//             None => None,
//         };
//         if delete {
//             self.map.remove(uuid);
//         }
//         result
//     }
// }

// impl Default for InMemory {
//     fn default() -> Self {
//         Self {
//             map: Arc::new(DashMap::with_capacity(100)),
//         }
//     }
// }
