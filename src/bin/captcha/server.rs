use actix_web::dev::Server;
use actix_web::{web, App, HttpServer, middleware};
use crossbeam::channel::bounded;
use std::env;
use tracing::info;

use api_captcha_rs::{storage, Captcha};
use crate::endpoints;

#[cfg(not(debug_assertions))]
static CAPTCHA_QUEUE_LEN: usize = 7;

#[cfg(debug_assertions)]
static CAPTCHA_QUEUE_LEN: usize = 1;


// pub fn create_server<T: storage::Storage + Send + Sync + 'static + Clone>(
pub fn create_server(
    // s: T,
) -> Result<Server, std::io::Error> {
    let port = env::var("NOMAD_PORT_http").unwrap_or(String::from("8080"));
    let addr = format!("0.0.0.0:{}", port);
    info!("{}, {}", env!("CARGO_PKG_NAME"), addr);

    let s = storage::Redis::default();

    let server = HttpServer::new(move || {
        let (tx, rx) = bounded::<Captcha>(CAPTCHA_QUEUE_LEN);
        let _sender = std::thread::spawn(move || {
            loop {
                match tx.send(api_captcha_rs::Captcha::default()) {
                    Err(_err) => break,
                    _ => {},
                }
            }
        });

        // let _guard = sentry::init("https://dda92933d12e4266937d354272f80b3c@sentry.pashinin.com/7");
        // let _guard = sentry::init((
        //     "https://dda92933d12e4266937d354272f80b3c@sentry.pashinin.com/7",
        //     sentry::ClientOptions {
        //         release: sentry::release_name!(),
        //         ..Default::default()
        //     },
        // ));
        // std::env::set_var("RUST_BACKTRACE", "1");

        App::new()
            .app_data(web::Data::new(rx))
            .app_data(web::Data::new(s.clone()))

            // .wrap(sentry_actix::Sentry::new())

            .wrap(common::cors()
                  .expose_headers(vec!["captcha-uuid"])
            )
            .wrap(middleware::DefaultHeaders::new().add(("Cache-control", "no-cache")))
            .wrap(middleware::Logger::default())


            .service(endpoints::healthcheck)
            // .route("/healthcheck", web::get().to(endpoints::healthcheck::<T>))

            // Deprecated
            // Used in old UI (it uses credentials in CORS)
            .service(
                web::resource("/")
                    .wrap(actix_web::middleware::Compat::new(common::cors().supports_credentials()))
                    .route(web::get().to(endpoints::base64_old))
            )

            // .route("/png", web::get().to(endpoints::png))
            .service(endpoints::png)
            .service(endpoints::as_base64)
    })
        .bind(addr).unwrap()
        .run();
    Ok(server)
}
