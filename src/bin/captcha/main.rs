//! Captcha microservice (HTTP server)
//!
//! Features:
//!
//! * Pre-generated captchas in each thread for ultra fast response
//! * Storage: Redis
//!
//!
//! Endpoints:
//!
//! * /base64
//! * /check/<captcha_id>
//! * /healthcheck
//! * /png

mod endpoints;
mod server;

// i64 because Duration() uses i64
static TTL: i64 = 60*15;

/// Starts HTTP server
#[actix_web::main]
async fn main () {
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    server::create_server().unwrap().await.unwrap();
}
