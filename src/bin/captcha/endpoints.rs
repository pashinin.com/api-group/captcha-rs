//! Endpoints for captcha API
//!
//! /healthcheck
//! /png
//! /base64

use actix_web::{web, Responder, get, HttpResponse, Error};
use base64;
use serde::{Serialize};
use std::env;
use chrono::Duration;
use crossbeam::channel::{Receiver};
use uuid::{Uuid};
use crate::TTL;
use api_captcha_rs::storage;


#[derive(Serialize)]
struct Info<'a> {
    service: &'a str,
    tag: &'a str,
    description: &'a str,
    commit_short_sha: &'a str,
    errors: Vec<String>,
}

/// Returns 200 if service is healthy
#[get("/healthcheck")]
pub async fn healthcheck(
    storage_redis: web::Data<storage::Redis>,
    // _user: User,
) -> impl Responder {
    let storage_ok = storage_redis.connection_ok();

    let mut info = Info{
        service: env!("CARGO_PKG_NAME"),
        tag: match option_env!("CI_COMMIT_TAG") {
            Some(tag) => tag,
            None => match option_env!("CI_COMMIT_REF_NAME") {
                Some(branch) => if branch == "master" { "latest" } else { branch },
                None => "unknown branch",
            },
        },
        description: env!("CARGO_PKG_DESCRIPTION"),
        commit_short_sha: match option_env!("CI_COMMIT_SHORT_SHA") {
            Some(sha) => sha,
            None => "",
        },
        errors: vec![],
    };

    // Check storage connection
    if !storage_ok.await {
        info.errors.push("Can't connect to Redis".to_string());
    }

    if info.errors.len() == 0 {
        HttpResponse::Ok().json(info)
    } else {
        HttpResponse::InternalServerError().json(info)
    }
}


/// Captcha as a PNG image
///
/// Pregenerated captcha will be taken from channel "rx".  They are
/// generated in separate thread (see `create_server` function)
#[get("/png")]
pub async fn png (
    storage: web::Data<storage::Redis>,
    rx: web::Data<Receiver<(String, Vec<u8>)>>,
) -> Result<HttpResponse, Error> {
    let captcha_uuid = Uuid::new_v4();

    // It will block and wait until a new captcha is generated if
    // channel is empty
    let (secret, image_bytes) = rx.recv().expect("Unable to receive from channel");

    storage.set(captcha_uuid, secret, Duration::seconds(TTL)).await;

    Ok(HttpResponse::Ok()
       .content_type("image/png")
       .insert_header(("captcha-uuid", captcha_uuid.to_string()))
       .body(image_bytes)
       .into())
}


/// Base64
#[get("/base64")]
pub async fn as_base64 (
    storage: web::Data<storage::Redis>,
    rx: web::Data<Receiver<(String, Vec<u8>)>>,
) -> Result<HttpResponse, Error> {
    let captcha_uuid = Uuid::new_v4();
    let (secret, image_bytes) = rx.recv().expect("Unable to receive from channel");
    storage.set(captcha_uuid, secret, Duration::seconds(TTL)).await;
    Ok(HttpResponse::Ok()
       .content_type("text/plain")
       .insert_header(("captcha-uuid", captcha_uuid.to_string()))
       .body(base64::encode(image_bytes))
       .into())
}


#[derive(Serialize)]
struct Base64Old {
    id: String,
    b64data: String,
}

/// Base64 in JSON
///
/// Result in JSON: {"id": <UUID>, "b64data": ...}  where b64data is a
/// string like this: "data:image/png;base64, <DATA>"
pub async fn base64_old (
    storage: web::Data<storage::Redis>,
    rx: web::Data<Receiver<(String, Vec<u8>)>>,
) -> Result<HttpResponse, Error> {
    let captcha_uuid = Uuid::new_v4();
    let (secret, image_bytes) = rx.recv().expect("Unable to receive from channel");
    storage.set(captcha_uuid, secret, Duration::seconds(TTL)).await;
    Ok(HttpResponse::Ok().json(Base64Old {
        id: captcha_uuid.to_string(),
        b64data: format!("data:image/png;base64, {}", base64::encode(image_bytes)),
    }))
}



// #[get("/check/{uuid}/{given_code}")]
// check
// pub(crate) async fn captcha_check<T: Storage> (
//     // in_memory: web::Data<InMemory>,
//     storage: web::Data<T>,
//     // db: web::Data<Arc<DashMap<Uuid, Item>>>,
//     // db: web::Data<InMemory>,
//     web::Path((captcha_uuid, given_code)): web::Path<(String, String)>
// ) -> impl Responder {
//     match Uuid::from_str(&captcha_uuid) {
//         Ok(id) => {
//             let secret = match storage.get(&id) {
//                 Some(data) => data,
//                 None => return HttpResponse::Ok().content_type("text/plain").body("no_such_captcha"),
//             };
//             // let diff = Utc::now() - item.expired_at;
//             // if diff >= Duration::seconds(TTL) {
//             //     return HttpResponse::Ok().content_type("text/plain").body("timeout");
//             // }
//             if *secret == given_code {
//                 HttpResponse::Ok().content_type("text/plain").body("ok")
//             } else {
//                 HttpResponse::Ok().content_type("text/plain").body("invalid_answer")
//             }
//         },
//         Err(err) => {
//             dbg!(err);
//             HttpResponse::Ok()
//                 .content_type("text/plain")
//                 .body("invalid_uuid")
//         }
//     }
// }
