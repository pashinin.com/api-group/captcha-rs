//! Captcha

use actix_web::{HttpRequest, Error, FromRequest, dev, web};
use serde::{Serialize, Deserialize};
use captchalib::filters::{Noise, Wave};
use core::future::Future;
use std::pin::Pin;
// use tracing::{info, error};

// Reexport
pub mod storage;
pub use storage::Redis;

// pub use common::models::Captcha;

// i64 because Duration() uses i64
static TTL: i64 = 60*15;

/// Captcha UUID and secret.
///
/// Some API requests need to include captcha.
#[derive(Serialize, Deserialize, Debug)]
pub struct Captcha {
    pub id: String,
    pub secret: String,

    #[serde(skip)]
    pub png_bytes: Option<Vec<u8>>,
}

impl Captcha {
    pub fn as_base64(&self) -> String {
        match &self.png_bytes {
            Some(data) => base64::encode(data),
            None => "".to_string()
        }
    }
}


impl Default for Captcha {
    fn default() -> Self {
        // (std::string::String, Vec<u8>)
        let (secret, image_bytes) = captchalib::Captcha::new()
            .set_chars(&"123456789".chars().collect::<Vec<char>>())
        // .set_color([245, 245, 245])
            .add_chars(4)
            .apply_filter(Noise::new(0.1))
            .apply_filter(Wave::new(-2.0, 10.0).horizontal())
            .view(150, 56)
            .as_tuple().unwrap();
        Captcha {
            id: uuid::Uuid::new_v4().to_string(),
            secret,
            png_bytes: Some(image_bytes),
        }
    }
}


impl FromRequest for Captcha {
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        // If you forget to pass a Storage - log an error and just
        // return an anonymous user.

        let captcha_rx = match req.app_data::<web::Data<crossbeam::channel::Receiver<Captcha>>>() {
            Some(storage) => storage.clone(),
            None => {
                // error!("You forgot to pass captcha::Storage into your Actix app.");
                panic!("You forgot to pass captcha::Storage into your Actix app.");
            }
        };
        let storage = match req.app_data::<web::Data<crate::storage::Redis>>() {
            Some(storage) => storage.clone(),
            None => {
                // error!("You forgot to pass captcha::Storage into your Actix app.");
                panic!("You forgot to pass captcha::Storage into your Actix app.");
            }
        };

        // let session = req.get_session();

        Box::pin(async move {
            // let captcha = Captcha::default();
            // let captcha_uuid = Uuid::new_v4();
            let captcha = captcha_rx.recv().expect("Unable to receive from channel");
            storage.set(&captcha.id, &captcha.secret, chrono::Duration::seconds(TTL)).await;
            Ok(captcha)
        })
    }
}
