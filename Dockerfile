FROM pashinin/rust-runtime:2021-01-26
COPY target/release/api-captcha-rs ./
ENTRYPOINT ["/api-captcha-rs"]
CMD ["./api-captcha-rs"]
